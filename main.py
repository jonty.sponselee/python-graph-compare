import graphs.matplotlib as matplotlib
import graphs.plotly as plotly
import graphs.seaborn as seaborn
import graphs.altair as altair
import graphs.bokeh as bokeh
import graphs.goopycharts as goopycharts

matplotlib.renderGraph()
plotly.renderGraph()
seaborn.renderGraph()
altair.renderGraph()
bokeh.renderGraph()
goopycharts.renderGraph()