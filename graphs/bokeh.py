from bokeh.io import output_file, show, export_png
from bokeh.plotting import figure


def renderGraph():
    output_file("./graphs/bokeh.html")

    fruits = ['Apples', 'Pears', 'Nectarines', 'Plums', 'Grapes', 'Strawberries']
    counts = [5, 3, 4, 2, 4, 6]

    p = figure(x_range=fruits)

    p.vbar(x=fruits, top=counts)

    show(p)
