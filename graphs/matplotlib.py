import numpy
import matplotlib.pyplot as plot


def renderGraph():
    # Data
    numpy.random.seed(3)
    x = 0.5 + numpy.arange(8)
    y = numpy.random.uniform(2, 7, len(x))

    # Plot
    fig, ax = plot.subplots()

    ax.bar(x, y, width=1, edgecolor="white", linewidth=0.7)

    ax.set(
        xlim=(0, 8), xticks=numpy.arange(1, 8),
        ylim=(0, 8), yticks=numpy.arange(1, 8)
    )

    plot.savefig("./graphs/matplot")

    plot.show()
