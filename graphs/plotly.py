# import plotly.io as plotly
import plotly.express as px

def renderGraph():
    data_canada = px.data.gapminder().query("country == 'Netherlands'")

    fig = px.bar(data_canada, x='year', y='pop')

    fig.show()
    fig.write_image("./graphs/plotly.png")