import seaborn as sns
import matplotlib.pyplot as plt


def renderGraph():
    sns.set_theme(style="whitegrid")
    tips = sns.load_dataset("tips")
    ax = sns.barplot(x="day", y="total_bill", data=tips)

    plt.savefig("./graphs/seaborn")
    plt.show()
